from django.contrib import admin

from .models import UserProfile

class UserProfileAdmin(admin.ModelAdmin):
	list_display = ['user_id', 'name']


admin.site.register(UserProfile,UserProfileAdmin)
