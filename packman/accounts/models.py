from __future__ import unicode_literals
import os, datetime
import pytz
from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver


def increment_account_id():
  last_account = UserProfile.objects.all().order_by('id').last()
  if not last_account:
    return 'RNH' + str(datetime.date.today().year) + str(datetime.date.today().month).zfill(2) + '0000'
  account_id = last_account.account_id
  account_id_int = int(account_id[10:14])
  new_account_id_int = account_id_int + 1
  new_account_id = 'RNH' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2) + str(new_account_id_int).zfill(4)
  return new_account_id

class UserProfile(models.Model):
  """docstring for UserProfile"""
  account_id = models.CharField(max_length = 20, default = increment_account_id, null = True, blank = True, editable=False)
  user = models.OneToOneField(User)
  name = models.CharField(max_length=300, null=False)
  address = models.CharField(max_length=300, null=False, blank=True)
  state = models.CharField(max_length=150, null=False, blank=True)
  country = CountryField( null=False, blank=True)
  phone = PhoneNumberField(null=False)

  # Status
  created = models.DateTimeField(auto_now_add=True)
  time = models.DateTimeField(auto_now=True)
  status = models.BooleanField(default=1, null=False)
  
User.Profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
