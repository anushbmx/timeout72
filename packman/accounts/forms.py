from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from models import UserProfile

class UserProfileForm(forms.ModelForm):
  address2 = forms.CharField(required=False)
  state = forms.CharField(required=False)
  phone = forms.CharField(widget=PhoneNumberPrefixWidget)

  class Meta:
    model = UserProfile
    fields = ('name', 'address', 'state', 'phone', 'country')

class renzoAuthenticationForm(AuthenticationForm):
  """docstring for renzoAuthenticationForm"""
  username = forms.CharField(required=True,
                              label=("Email"),
                              error_messages={
                                'required' : ("Email Address Cannot be empty"),
                              })

class renzoUserCreationForm(UserCreationForm):
  """docstring for renzoUserCreationForm"""
  username = forms.CharField(required=True,
                              label=("Email"),
                              error_messages={
                                'required' : ("Email Address Cannot be empty"),
                              })
  # Custom Error Messages
  error_messages = {
    'duplicate_username': ("User account already exist"),
    'password_mismatch' : ("Two Passwords does not match")
  }
  class Meta:
    model = User
    fields = ('username',)

  def save(self, commit=True):
    user = super(renzoUserCreationForm, self).save(commit=False)
    # Application uses same value for username and email address
    user.email = self.cleaned_data['username']

    if commit:
      user.save()

    return user

  def clean_username(self):
    # Since the app uses single value for username and email address 
    # it's necessary to check username for uniqueness
    username = self.cleaned_data["username"]
    if self.instance.username == username:
      return username
    try:
      User._default_manager.get(username=username)
    except User.DoesNotExist:
      return username
    raise forms.ValidationError(self.error_messages['duplicate_username'],
                                code='duplicate_username',)
