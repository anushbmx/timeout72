from django.shortcuts import render
from django.contrib import auth
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect

from django.contrib import auth
from decorators import login_required, logout_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.core.urlresolvers import reverse


from forms import renzoUserCreationForm, renzoAuthenticationForm, UserProfileForm
from bookings.models import Booking
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth.models import User, Group
from django.contrib.auth import get_user_model

from bookinglist.models import BookingList
from django.contrib.auth.decorators import user_passes_test

from bookings.utils import BookingFilter
# Todo
#   1. Add Registered success page or
#       1. Register
#   2. Add Password reset/forgot password form 
#  

@login_required
def booking_info(request, booking_id):    
  data = {}
  # Grab active property linked to the user
  booking = get_object_or_404(Booking, booking_id=booking_id, status=True)
  order = get_object_or_404(Order, booking=booking)
  # Booking Not Locked
  if order.lock == 0:
    return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))
  order_items = OrderItems.objects.filter(order=order, status=1)
  payments = Payments.objects.filter(booking=booking, order=order)
  data['payments'] = payments
  data['order'] = order
  data['order_items'] = order_items
  data['booking'] = booking
  data['title'] = 'Booking ' + str(booking_id)
  return render(request, 'accounts/booking_info.html', data) 

@login_required
def bookinglist_list(request):
  data = {}

  bookings = BookingList.objects.order_by('-created')

  to_show = 20
  paginator = Paginator(bookings, to_show) # Show 25 contacts per page
  count = bookings.count
  page = request.GET.get('page')
  try:
      bookings = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      bookings = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      bookings = paginator.page(paginator.num_pages)

  data['count'] = count
  data['bookings'] = bookings
  data['title'] = "Bookings"
  return render(request, 'accounts/bookinglist.html', data)

@login_required
@user_passes_test(lambda u: u.is_superuser)
def booking_list(request):
  data = {}

  bookings = BookingFilter(request.GET, queryset=Booking.objects.order_by('-created'))

  to_show = 100
  paginator = Paginator(bookings, to_show) # Show 25 contacts per page
  count = bookings.count
  page = request.GET.get('page')
  try:
      bookings = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      bookings = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      bookings = paginator.page(paginator.num_pages)

  data['count'] = count
  data['bookings'] = bookings
  data['title'] = "Bookings"
  return render(request, 'accounts/booking_list.html', data)

@login_required
def changePassword(request):
  form = form = PasswordChangeForm(user=request.user, data=request.POST or None)
  data = {}

  if request.method == 'POST':
    form = PasswordChangeForm(user=request.user, data=request.POST)
    if form.is_valid():
      form.save()
      update_session_auth_hash(request, form.user)
      data['message'] = 'password changed successfully'
    else : 
      data['errors'] = form.errors

  data['changepasswordform'] = form
  data['title'] = 'Change Password'
  return render(request,'accounts/changepassword.html', data)

@login_required
def index(request):
  data = {}
  data['user_info'] = request.user.Profile
  return render(request,'accounts/index.html', data)

@login_required
def profile(request):
  form = UserProfileForm(request.POST or None, instance=request.user.Profile)
  data = {}

  if request.method == 'POST':
    if form.is_valid():
      form.save()
    else :
      data['errors'] = form.errors

  data['userprofileform'] = form
  data['title'] = 'Profile'
  return render(request,'accounts/profile.html', data)

@logout_required
def login(request):
  data = {}
  form = renzoAuthenticationForm(request.POST or None)
  if request.method == 'POST' and form.is_valid:
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None:
      auth.login(request, user)
      return HttpResponseRedirect(reverse('accounts.index'))
    else:
      data['errors'] = ("Invalid Login")
  else:
    data['errors'] = form.errors

  data['authenticationForm'] = form
  data['title'] = 'Login'
  return render(request,'accounts/login.html', data)

@login_required
def logout(request):
  auth.logout(request)
  return HttpResponseRedirect('/accounts/login/')

@logout_required
def register(request):
  form = renzoUserCreationForm(request.POST or None)
  data = {}

  if request.method == 'POST':
    if form.is_valid():
      form.save()
      return HttpResponseRedirect('/accounts/')
    else :
      data['errors'] = form.errors

  data['usercreationform'] = form
  data['title'] = 'Register'
  return render(request,'accounts/register.html', data)
