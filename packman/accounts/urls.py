from django.conf.urls import url

from accounts import views

urlpatterns = [
  url(r'^$', views.index, name='accounts.index'),
  url(r'login/$', views.login, name='accounts.login'),
  url(r'register/$', views.register, name='accounts.register'),
  url(r'profile/$', views.profile, name='accounts.profile'),
  url(r'logout/$', views.logout, name='accounts.logout'),
  url(r'changepassword/$', views.changePassword, name='accounts.changepassword'),

  url(r'renzo-bookings/$', views.booking_list, name='accounts.bookings.list'),
  url(r'bookings/$', views.bookinglist_list, name='accounts.bookinglist.list'),


]
