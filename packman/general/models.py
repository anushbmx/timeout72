from __future__ import unicode_literals
import datetime
import os
from binascii import hexlify
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

class Campagin(models.Model):
  start_date = models.DateField(null=False, default=datetime.date.today)

class Promo(models.Model):
  promo_code = models.CharField(max_length=200, null=False, unique=True)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  description = models.CharField(max_length=400, default="", null=False)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

class Addon(models.Model):
  CHOICES = [(i,i) for i in range(1, 20)]

  name = models.CharField(max_length=200, null=False)
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  quantity = models.IntegerField(default=-1, null=False, help_text="-1 For unlimited quantity")
  description = models.CharField(max_length=400, default="", null=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s (INR %s)' % (self.name, self.price)

class PackageLength(models.Model):
  CHOICES = [(i,i) for i in range(1, 20)]
  nights = models.IntegerField(default=1, null=False, choices=CHOICES, unique=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s Nights' % (self.nights)

class Amenity(models.Model):
  name = models.CharField(max_length=200, null=False, unique=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s : %s' % (self.id, self.name)

class PropertyType(models.Model):
  name = models.CharField(max_length=200, null=False, unique=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s' % (self.name)

class City(models.Model):
  name = models.CharField(max_length=200, null=False, unique=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s' % (self.name)

def _createId():
  return hexlify(os.urandom(16))

class Host(models.Model):
  name = models.CharField(max_length=300, null=False)
  phone = PhoneNumberField(null=False)
  email = models.EmailField(max_length=500, null=False)
  hash = models.CharField(max_length=32, default=_createId, editable=False)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s : %s' % (self.id, self.name)

class Guest(models.Model):
  name = models.CharField(max_length=300, null=False)
  phone = PhoneNumberField(null=False)
  email = models.EmailField(max_length=500, null=False)
  hash = models.CharField(max_length=32, default=_createId, editable=False)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s : %s' % (self.id, self.name)