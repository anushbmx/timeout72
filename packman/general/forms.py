from django import forms
from .models import Guest
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget


class guest_add_form(forms.ModelForm):
  phone = forms.CharField(widget=PhoneNumberPrefixWidget)

  class Meta:
    model = Guest
    fields = ('email', 'phone', 'name')


class guest_auth_form(forms.Form):
  phone = forms.CharField(widget=PhoneNumberPrefixWidget)
  email = forms.EmailField()