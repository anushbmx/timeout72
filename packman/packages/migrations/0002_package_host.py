# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-27 19:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0002_host'),
        ('packages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='host',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='general.Host'),
            preserve_default=False,
        ),
    ]
