import datetime
from .models import Package
from bookings.models import Order, OrderItems, Booking
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from bookings.forms import booking_add_form
from general.forms import guest_add_form
from general.models import Guest, Addon
from django.http import HttpResponse, HttpResponseRedirect,Http404
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from utls import ProductFilter

# Create your views here.


def package_list(request):
  data = {}
  # Grab active property linked to the user
  packages = ProductFilter(request.GET, queryset=Package.objects.filter(status=1))
  to_show = 9
  paginator = Paginator(packages, to_show) # Show 25 contacts per page
  count = packages.count
  page = request.GET.get('page')
  try:
      packages = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      packages = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      packages = paginator.page(paginator.num_pages)

  data['count'] = count
  data['packages'] = packages
  data['title'] = 'All Packages'
  return render(request, 'packages/packages_list.html', data)


def package_info(request, package_id):
  data = {}
  package = get_object_or_404(Package, id=package_id, status=True)
  form = booking_add_form(request.POST or None, package=package, prefix='booking')
  guest_form = guest_add_form(request.POST or None, prefix='Guest')
  add_ons = Addon.objects.filter(status=1)
  if form.is_valid() and guest_form.is_valid() :
    guest, true = Guest.objects.get_or_create(email=guest_form.cleaned_data['email'], 
                    phone=guest_form.cleaned_data['phone'],
                    defaults={
                        'name': guest_form.cleaned_data['name']
                    },
                    status=True)
    booking = form.save(commit=False)
    booking.guest = guest
    # booking.save()
    checkin = datetime.datetime.strptime(str(form.cleaned_data['checkin']), "%Y-%m-%d").date()
    checkout = checkin + datetime.timedelta(hours=booking.length.nights*24)
    booking.checkout = checkout
    booking.package = package
    booking.save()
    # create Order
    order = Order.objects.create(booking=booking)

    # Create Order items
    description = 'Booking "' + str(package.name) + '" for 1 Night'
    OrderItems.objects.create(
        order=order,
        booking=booking,
        price=package.base_price,
        quantity=booking.length.nights,
        description=description
        # Tax calculation here
        # tax = tax
      )

    if booking.no_of_extra_bed > 0 :
      description = str(booking.no_of_extra_bed) + " x Additional Guest for 1 Night"
      OrderItems.objects.create(
      order=order,
      booking=booking,
      price=package.extra_bed_price * booking.no_of_extra_bed,
      quantity=booking.length.nights,
      description=description
      # Tax calculation here
      # tax = tax
    )
    request.session['user_hash'] = guest.hash
    return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))
  else :
    data['errors'] = str(form.errors)  + str(guest_form.errors)

  data['form'] = form
  data['guest_form'] = guest_form
  data['package'] = package
  data['add_ons'] = add_ons
  data['title'] = str(package.name)
  return render(request, 'packages/packages_info.html', data)