from django.contrib import admin
from .models import *
# Register your models here.

def disable_packages(modeladmin, request, queryset):
    queryset.update(status=False)
disable_packages.short_description = "Mark selected packges as disabled"

class PackageAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'phone']
	actions = [disable_packages]

admin.site.register(Package,PackageAdmin)
