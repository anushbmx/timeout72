import django_filters
from .models import Package

class ProductFilter(django_filters.FilterSet):
  nights = django_filters.NumberFilter(name='length__nights', lookup_expr='exact')
  city = django_filters.CharFilter(name='city__name', lookup_expr='icontains')
  price_lt = django_filters.NumberFilter(name='base_price', lookup_expr='lte')
  price_gt = django_filters.NumberFilter(name='base_price', lookup_expr='gte')
  class Meta:
      model = Package
      fields = ['state', 'nights', 'city', 'price_lt', 'price_gt']