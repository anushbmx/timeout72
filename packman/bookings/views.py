import datetime
import hashlib
from .models import Booking, Order, OrderItems, Payments
from .forms import order_item_add_form, order_promo_add_form, order_confirm_form, booking_email_form
from django.forms.forms import NON_FIELD_ERRORS
from django.views.decorators.csrf import csrf_exempt
from general.models import Promo, Addon, Guest
from general.forms import guest_auth_form
from django.shortcuts import render
from decimal import Decimal
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect,Http404
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.conf import settings
from .utils import email_guest, email_host
from django.core.exceptions import ObjectDoesNotExist
# Create your views here.


def booking_info(request, booking_id):
  if request.session.get('user_hash') is None and not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('booking.auth'))

  data = {}
  if not request.user.is_authenticated():
    guest = get_object_or_404(Guest, hash=request.session['user_hash'])
  # Grab active property linked to the user
  booking = get_object_or_404(Booking, booking_id=booking_id, status=True)
  order = get_object_or_404(Order, booking=booking)
  # Booking Not Locked
  if order.lock == 0:
    return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))
  order_items = OrderItems.objects.filter(order=order, status=1)
  payments = Payments.objects.filter(booking=booking, order=order)

  #Email Booking
  email_form = booking_email_form(request.POST or None, prefix='email_form', initial={'email':booking.guest.email})
  if request.method == 'POST':
    if email_form.is_valid():
      email_guest(booking, email_form.cleaned_data['email'])
      email_form.add_error(None, "Email send to " + email_form.cleaned_data['email'])
    elif request.POST.get('email_form', None)  :
      data['errors'] = email_form.errors
    else:
      email_form.add_error(None, "Invalid Email.")

  data['payments'] = payments
  data['order'] = order
  data['email_form'] = email_form
  data['order_items'] = order_items
  data['booking'] = booking
  data['title'] = 'Booking ' + str(booking_id)
  return render(request, 'booking/booking_info.html', data)

def booking_review(request, booking_id):
  if request.session.get('user_hash') is None and not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('booking.auth'))

  data = {}
  if not request.user.is_authenticated():
    guest = get_object_or_404(Guest, hash=request.session['user_hash'])

  booking = get_object_or_404(Booking, booking_id=booking_id, status=True)
  order = get_object_or_404(Order, booking=booking, lock=0)
  order_items = OrderItems.objects.filter(order=order, status=1)
  add_ons = Addon.objects.filter(status=1)
  addon_form = order_item_add_form(request.POST or None, prefix='addon_form')
  if request.method == 'POST':
    if addon_form.is_valid():
      order_item = addon_form.save(commit=False)
      order_item.description = order_item.add_on.name
      order_item.price = order_item.add_on.price
      order_item.order=order
      order_item.lock = False
      order_item.save()
      return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))
    elif request.POST.get('addon_form', None)  :
      data['errors'] = addon_form.errors

  promo_form = order_promo_add_form(request.POST or None, prefix='promo_form')
  if request.method == 'POST':
    if promo_form.is_valid():
      promo = Promo.objects.filter(promo_code=promo_form.cleaned_data['promo_code'])
      if promo.count() == 1:
        order.promo = promo[0]
        order.save()
        return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))
      else :
        promo_form.add_error(None, "Invalid Promo Code")
    elif request.POST.get('promo_form', None)  :
      data['errors'] = promo_form.errors

  order_form = order_confirm_form(request.POST or None, prefix="order_form")
  if request.method == 'POST':
    if order_form.is_valid():
      if int(order_form.cleaned_data['payment_type']) == 1 :
        order.payment_type = 1
        total_50_1 = order.total() * 0.5
        total_50_2 = order.total() * 0.5
        first_payment = Payments.objects.create(
          booking=booking,
          order=order,
          total=Decimal(total_50_1)
        )

        Payments.objects.create(
          booking=booking,
          order=order,
          due='2017-12-15',
          total=Decimal(total_50_2)
        )
      else :
        order.payment_type = 0
        total_dec = order.total()
        first_payment = Payments.objects.create(
          booking=booking,
          order=order,
          total=Decimal(total_dec)
        )
      # Update Order
      order_items.update(lock=1)
      order.lock=1
      order.save()
      return HttpResponseRedirect(reverse('booking.payment', kwargs={'payment_hash':first_payment.hash}))
    elif order_form.cleaned_data.get('order_form', None)  :
      data['errors'] = order_form.errors

  data['total_50'] = order.total() * 0.5
  data['total_50_date'] = '2017-12-15'
  data['add_ons'] = add_ons
  data['order_form'] = order_form
  data['promo_form'] = promo_form
  data['addon_form'] = addon_form
  data['order'] = order
  data['order_items'] = order_items
  data['booking'] = booking
  data['title'] = 'Booking ' + str(booking_id)
  return render(request, 'booking/booking_review.html', data)

def booking_order_item_remove(request, booking_id, order_id, order_item_id):
  data = {}
  booking = get_object_or_404(Booking, id=booking_id, status=True)
  order = get_object_or_404(Order, booking=booking, id=order_id, lock=False)
  order_item = get_object_or_404(OrderItems, order=order, id=order_item_id)
  order_item.status = 0
  order_item.save()
  return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))

def booking_promo_code_remove(request, booking_id, order_id, promo_id):
  data = {}
  booking = get_object_or_404(Booking, id=booking_id, status=True)
  order = get_object_or_404(Order, booking=booking, id=order_id, promo=promo_id, lock=False)
  order.promo = None
  order.save()
  return HttpResponseRedirect(reverse('booking.review', kwargs={'booking_id':booking.booking_id}))


def booking_payment(request, payment_hash):
  data = {}
  payment = get_object_or_404(Payments, hash=payment_hash, status=True)
  productinfo = "Roomnhouse Booking " + str(payment.booking.booking_id)
  hash_item = str(settings.PAYMENT_GATEWAY['GATEWAYKEY']) + '|' + str(payment.invoice_no) + '|' + str(payment.total) + '|' + str(productinfo) + '|' + str(payment.booking.guest.name) + '|' + str(payment.booking.guest.email) + '|' +'|'+ '|' +'|'+ '|' +'|'+ '|' +'|'+ '|' +'|'+ '|' + str(settings.PAYMENT_GATEWAY['GATEWAYSALT']);
  data['productinfo'] = productinfo
  data['navigation'] = False
  data['footer'] = False
  data['booking_hash'] = hashlib.sha512(hash_item).hexdigest().lower()
  data['payment'] = payment
  data['booking'] = payment.booking
  data['gateway'] = settings.PAYMENT_GATEWAY
  data['title'] = "Payment : " + str(payment.id)
  return render(request, 'booking/payment.html', data)

@csrf_exempt
def booking_payment_success(request):
  if request.method == 'POST':
    data = {}
    payment = get_object_or_404(Payments, invoice_no=request.POST.get('txnid'), status=True)
    productinfo = "Roomnhouse Booking " + str(payment.booking.booking_id)
    hash_item = str(settings.PAYMENT_GATEWAY['GATEWAYSALT']) + '|' + str(request.POST.get('status')) + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + str(payment.booking.guest.email) + '|' + str(payment.booking.guest.name) + '|' + str(productinfo) + '|' + str(payment.total) + '|' + str(payment.invoice_no) + '|' + str(settings.PAYMENT_GATEWAY['GATEWAYKEY']) 
    hash_value = hashlib.sha512(hash_item)
    if hash_value.hexdigest().lower() == request.POST.get('hash') :
      payment.payment_gateway_type = request.POST.get('PG_TYPE')
      payment.transaction_date_time = request.POST.get('addedon')
      payment.mode = request.POST.get('mode')
      payment.payment_status = request.POST.get('status')
      payment.amount = request.POST.get('amount')
      payment.mihpayid = request.POST.get('mihpayid')
      payment.bankcode = request.POST.get('bankcode')
      payment.bank_ref_num = request.POST.get('bank_ref_num')
      payment.discount = request.POST.get('discount')
      payment.additional_charges = request.POST.get('additionalCharges', 0)
      payment.txn_status_on_payu = request.POST.get('unmappedstatus')
      payment.hash_status = "Success" if hash_value.hexdigest().lower() == request.POST.get('hash') else "Failed"
      payment.payment_status = Payments.PAID
      payment.save()
      booking = payment.booking
      if str(payment.order.payment_left()) == '0.0':
        booking.booking_status = Booking.CONFIRMED
      else :
        booking.booking_status = Booking.BLOCKED
      booking.save()
      email_guest(booking)
      #email_host(booking)
      return HttpResponseRedirect(reverse('booking.payment', kwargs={'payment_hash':payment.hash}))
    else :
      return HttpResponseRedirect(reverse('booking.payment.error'))
  else :
    return HttpResponseRedirect(reverse('booking.payment.error'))

@csrf_exempt
def booking_payment_error(request):
  data = {}
  data['navigation'] = False
  data['footer'] = False
  data['title'] = "Payment Failed"
  return render(request, 'booking/payment_error.html', data)

@csrf_exempt
def booking_payment_cancel(request):
  data = {}
  data['navigation'] = False
  data['footer'] = False
  data['title'] = "Payment Cancelled"
  return render(request, 'booking/payment_cancel.html', data)

def booking_auth(request):
  data = {}
  request.session['user_hash'] = None
  form = guest_auth_form(request.POST or None)
  if request.method == "POST":
    if form.is_valid():
      try:
        print 'sad'
        guest = Guest.objects.get(email=form.cleaned_data['email'], phone=form.cleaned_data['phone'])
        request.session['user_hash'] = guest.hash
        return HttpResponseRedirect(reverse('booking.list'))
      except ObjectDoesNotExist:
        form.add_error(None, "Invalid Credentials")
  data['form'] = form
  data['navigation'] = False
  data['footer'] = False
  data['title'] = "Retrive Booking"
  return render(request, 'booking/auth.html', data)

def booking_list(request):
  data = {}
  if request.session.get('user_hash') is None:
    return HttpResponseRedirect(reverse('booking.auth'))

  guest = get_object_or_404(Guest, hash=request.session['user_hash'])
  bookings = Booking.objects.filter(guest=guest).order_by('-created')
  data['bookings'] = bookings
  data['guest'] = guest
  data['title'] = "Your Bookings"
  return render(request, 'booking/booking_list.html', data)