import django_filters
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
from .models import Order, Payments, OrderItems, Booking

class BookingFilter(django_filters.FilterSet):
  booking_status = django_filters.NumberFilter(name='booking_status', lookup_expr='exact')
  class Meta:
      model = Booking
      fields = ['booking_status',]

def email_guest(booking=None, email=None):
  if booking is not None:
    data = {}
    order = Order.objects.get(booking=booking)
    payments = Payments.objects.filter(booking=booking, order=order)
    order_items = OrderItems.objects.filter(order=order, status=1, booking=None)
    data['order_items'] = order_items
    data['booking'] = booking
    data['order'] = order
    data['payments'] = payments
    data['gateway'] = settings.PAYMENT_GATEWAY
    subject = "Roomnhouse Booking " + str(booking.booking_id)
    if order.payment_left() == 0.00:
      body = render_to_string('booking/email/guest_confirm.ltxt', data)
    else :
      body = render_to_string('booking/email/guest_blocked.ltxt', data)
    if email is not None:
      email = [email]
    else :
      email = [booking.guest.email]
    email = EmailMessage(subject, body, 'support@roomnhouse.com', email)
    email.send()


def email_host(booking=None, email=None):
  if booking is not None:
    data = {}
    order = Order.objects.get(booking=booking)
    data['booking'] = booking
    data['order'] = order
    data['gateway'] = settings.PAYMENT_GATEWAY
    subject = "Roomnhouse Booking " + str(booking.booking_id)
    body = render_to_string('booking/email/host.ltxt', data)
    if email is not None:
      email_to = [email]
    else :
      email = [booking.host.email]
    email = EmailMessage(subject, body, 'support@roomnhouse.com', email)
    email.send()