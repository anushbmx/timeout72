from django.contrib import admin
from .models import *
# Register your models here.


class BookingAdmin(admin.ModelAdmin):
	list_display = ['id', 'booking_id', 'checkin', 'checkout', 'package']

class OrderAdmin(admin.ModelAdmin):
	list_display = ['id' ,'booking', 'total']

class PaymentsAdmin(admin.ModelAdmin):
	list_display = ['id', 'hash', 'invoice_no', 'booking', 'order', 'total', 'payment_status']

admin.site.register(Payments,PaymentsAdmin)
admin.site.register(Booking,BookingAdmin)
admin.site.register(Order,OrderAdmin)