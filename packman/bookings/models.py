from __future__ import unicode_literals
import os, datetime
from binascii import hexlify
from decimal import Decimal
from django.db import models
from general.models import PackageLength, Guest, Promo, Addon
from packages.models import Package

# Create your models here.

def increment_booking_number():
  last_booking = Booking.objects.all().order_by('id').last()
  if not last_booking:
    return 'RNH' + str(datetime.date.today().year) + str(datetime.date.today().month).zfill(2) + '0000'
  booking_id = last_booking.booking_id
  booking_int = int(booking_id[9:13])
  new_booking_int = booking_int + 1
  new_booking_id = 'RNH' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2) + str(new_booking_int).zfill(4)
  return new_booking_id

class Booking(models.Model):
  CHOICES = [(i,i) for i in range(0,10)]
  VOID = -1
  NONE = 0
  CONFIRMED = 1
  BLOCKED =2
  BOOKING_STATUS = (
    (VOID, 'Void'),
    (NONE, 'Not Confirmed'),
    (CONFIRMED, 'Confirm'),
    (BLOCKED, 'Blocked'),
  )
  booking_id = models.CharField(max_length = 20, default = increment_booking_number, null = True, blank = True, editable=False)
  checkin = models.DateField(null=False)
  checkout = models.DateField(null=False)
  length = models.ForeignKey(PackageLength, on_delete=models.PROTECT)
  package = models.ForeignKey(Package, on_delete=models.PROTECT)
  quantity = models.IntegerField(default=1, null=False, choices=CHOICES)

  booking_status = models.IntegerField(default=NONE, choices=BOOKING_STATUS, null=False)
  no_of_guest = models.IntegerField(default=1, null=False, choices=CHOICES)
  no_of_extra_bed = models.IntegerField(default=0, null=False, choices=CHOICES)
  guest = models.ForeignKey(Guest, on_delete=models.PROTECT)
  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def no_of_nights(self):
    delta =  self.checkout -  self.checkin
    no_of_night = int(delta.days)
    return no_of_night

  def __str__(self):
    return '%s' % (self.id)

class Order(models.Model):
  """ Order Model """
  ACTIVE = 1
  NULL = 0
  ORDER_STATUS = (
    (1, 'Active'),
    (0, 'Null')
  )
  CASH = 0
  BANK_TRANSFER = 1
  ONLINE_PAYMENT =2
  PAYMENT_METHOD = (
    (CASH, 'Cash'),
    (BANK_TRANSFER, 'Bank Transfer'),
    (ONLINE_PAYMENT, 'Online Payment'),
  )

  OPTIONS = (
    (0, 'Full Payment'),
    (1, 'Part Payment'),
  )
  # All booking, will be added to Invoice including parent
  booking = models.ForeignKey(Booking, on_delete=models.PROTECT,editable=False)

  promo = models.ForeignKey(Promo, on_delete=models.PROTECT, null=True)

  lock = models.BooleanField(default=False, null=False)
  order_status = models.IntegerField(default=1, choices=ORDER_STATUS, null=False)
  payment_method = models.IntegerField(default=0, choices=PAYMENT_METHOD, null=False)
  payment_type = models.IntegerField(choices=OPTIONS, default=0, null=False)

  # Records
  created = models.DateTimeField(auto_now_add=True) 
  time = models.DateTimeField(auto_now=True)
  status = models.IntegerField(default=1, null=False)

  class Meta:
    ordering = ('-created','-id')
  def __str__(self):
    return '%s' % (self.id)

  def payment_total(self):
    total = Decimal('0.00')
    for payment in self.payments.all().filter(payment_status=Payments.PAID):
      total = total + payment.total
    return round(total,2)

  def payment_left(self):
    return round(self.total() - self.payment_total(),2)

  def sub_total(self):
    total = Decimal('0.00')
    for item in self.items.all().exclude(status=0):
      total = total + item.total()
    return round(total,2)

  def charge(self):
    charge = self.sub_total() * 0.08
    return round(charge,2)

  def tax(self):
    tax = self.charge() * 0.15
    return round(tax,2)

  # Add Tax and service  
  def total(self):
    if self.promo: 
      return (self.sub_total() + self.charge() + self.tax() - round(self.promo.price,2)) 
    else : 
      return (self.sub_total() + self.charge() + self.tax()) 
    
class OrderItems(models.Model):
  order = models.ForeignKey(Order, on_delete=models.PROTECT, related_name='items', null=True)
  description = models.CharField(max_length=500, null=True)

  booking = models.ForeignKey(Booking, on_delete=models.PROTECT, null=True)
  add_on = models.ForeignKey(Addon, on_delete=models.PROTECT, null=True)
  lock = models.BooleanField(default=True, null=False)

  # Price 
  price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  quantity = models.IntegerField(default=1, null=False)
  tax = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
  discount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

  # Records
  created = models.DateTimeField(auto_now_add=True) 
  time = models.DateTimeField(auto_now=True)
  status = models.IntegerField(default=1, null=False)

  def __str__(self):
    return '%s' % (self.id)

  def total(self):
    total = Decimal( str( (self.price * self.quantity) - self.discount ))
    return total.quantize(Decimal('0.01'))

def _createId():
  return hexlify(os.urandom(16))
  

def increment_invoice_number():
  last_invoice = Payments.objects.all().order_by('id').last()
  if not last_invoice:
    return 'INV' + str(datetime.date.today().year) + str(datetime.date.today().month).zfill(2) + '0000'
  invoice_no = last_invoice.invoice_no
  invoice_int = int(invoice_no[9:13])
  new_invoice_int = invoice_int + 1
  new_invoice_no = 'INV' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2) + str(new_invoice_int).zfill(4)
  return new_invoice_no

class Payments(models.Model):
  CASH = 0
  BANK_TRANSFER = 1
  ONLINE_PAYMENT =2
  PAYMENT_METHOD = (
    (CASH, 'Cash'),
    (BANK_TRANSFER, 'Bank Transfer'),
    (ONLINE_PAYMENT, 'Online Payment'),
  )

  NOTPAID = 0
  PAID = 1
  FAILED = -1
  PAYMENT_STATUS = (
    (NOTPAID, 'Not Paid'),
    (PAID, 'Paid'),
    (FAILED, 'Failed'),
  )
  invoice_no = models.CharField(max_length = 20, default = increment_invoice_number, null = True, blank = True, editable=False)
  hash = models.CharField(max_length=32, default=_createId, editable=False)
  void = models.BooleanField(default=False, null=False)
  booking = models.ForeignKey(Booking, on_delete=models.PROTECT)
  order = models.ForeignKey(Order, on_delete=models.PROTECT, related_name='payments')
  total = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
  payment_method = models.IntegerField(default=2, choices=PAYMENT_METHOD, null=False)
  payment_status = models.IntegerField(default=0, choices=PAYMENT_STATUS, null=False)

  due = models.DateField(null=True, blank=True)

  # Gateway Record
  # Payment gateway type used in transaction
  payment_gateway_type = models.CharField(max_length=20, null=True, blank=True)  # Map to PG_TYPE

  # Map to addedon
  transaction_date_time = models.DateTimeField(null=True, blank=True)

  # mode  (credit card/ CD - Cheque / Net Banking)
  mode = models.CharField(max_length=10, null=True, blank=True)   
  gateway_status = models.CharField(max_length=15, null=True, blank=True)
  amount = models.DecimalField(max_digits=19, decimal_places=2, default=0.00)

  # Unique id from PayU.in
  mihpayid = models.CharField(max_length=100, null=True, blank=True)
  bankcode = models.CharField(max_length=10, null=True, blank=True)

  # Reference number for the payment gateway (received in PG_TYPE)
  bank_ref_num = models.CharField(max_length=100, null=True, blank=True)   
  discount = models.DecimalField(max_digits=19, decimal_places=6, default=0)   
  additional_charges = models.DecimalField(max_digits=19, decimal_places=6, default=0) # Charged by Payu

  # Status of transaction in PayU system
  # Map to unmappedstatus(initiated/ in progress /dropped / bounced / captured / auth/ failed / usercancelled/ pending)
  txn_status_on_payu = models.CharField(max_length=20, null=True, blank=True)  
  hash_status = models.CharField(max_length=100, null=True, blank=True)

  # Records
  created = models.DateTimeField(auto_now_add=True) 
  time = models.DateTimeField(auto_now=True)
  status = models.IntegerField(default=1, null=False)