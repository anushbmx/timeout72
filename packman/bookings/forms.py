import datetime
from django import forms
from .models import Booking, Order, OrderItems

class booking_email_form(forms.Form):
  email = forms.EmailField(required=True, label="Email", max_length="100", widget=forms.TextInput(attrs={'class':'input-group-field'}))
  email_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)

class booking_add_form(forms.ModelForm):
  campagin_start = datetime.datetime.strptime('12-25-2017', "%m-%d-%Y").date()
  campagin_end = datetime.datetime.strptime('01-01-2018', "%m-%d-%Y").date()

  if datetime.date.today() > campagin_start  :
    start_date = datetime.date.today()
  else :
    start_date = campagin_start

  delta =  campagin_end -  campagin_start
  campagin_days = int(delta.days)

  DATECHOICE=[(campagin_start + datetime.timedelta(hours=date*24), start_date + datetime.timedelta(hours=date*24)) for date in range(0,campagin_days)]
  checkin = forms.ChoiceField(choices=DATECHOICE)


  def __init__(self, *args, **kwargs):
    package = kwargs.pop('package', None)
    super(booking_add_form, self).__init__(*args, **kwargs)
    if package is not None:
      self.fields['length']  =  forms.ModelChoiceField(label="Package",queryset=package.length.all(), widget=forms.RadioSelect(),empty_label=None)
      self.fields['no_of_guest'] = forms.ChoiceField(choices= [(i,i) for i in range(1,package.base_occupancy+1)])
      self.fields['no_of_extra_bed'] = forms.ChoiceField(label="Extra *", choices=[(0, 'None')] + [(i,i) for i in range(1,package.number_of_extra_bed+1)], required=False)

  class Meta:
    model = Booking
    fields = ('checkin','length', 'no_of_guest', 'no_of_extra_bed')

class order_item_add_form(forms.ModelForm):
  CHOICES = [(i,i) for i in range(1, 10)]
  quantity = forms.ChoiceField(choices=CHOICES)
  addon_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)

  def __init__(self, *args, **kwargs):
    super(order_item_add_form, self).__init__(*args, **kwargs)
    self.fields['add_on'].label = "Avalible Add-on"
    self.fields['add_on'].empty_label = None
    
  class Meta:
    model = OrderItems
    fields =('add_on','quantity')
    widgets = {
        'add_on': forms.RadioSelect(),
    }

class order_promo_add_form(forms.Form):
  promo_code = forms.CharField(required=True, label="Promo Code", max_length="20", widget=forms.TextInput(attrs={'class':'input-group-field'}))
  promo_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)

class order_confirm_form(forms.Form):
  OPTIONS = (
    (0, 'Full Payment'),
    (1, 'Part Payment'),
  )
  #payment_type = forms.ChoiceField(choices=OPTIONS, widget=forms.RadioSelect(), required=True)
  payment_type = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=0)
  order_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)