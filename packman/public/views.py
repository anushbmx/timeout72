from django.shortcuts import render
from packages.models import Package
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def home(request):
  data = {}
  packages = Package.objects.filter(status=1).order_by('-featured')
  paginator = Paginator(packages, 8) # Show 25 contacts per page

  page = request.GET.get('page')
  try:
      packages = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      packages = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      packages = paginator.page(paginator.num_pages)

  data['packages'] = packages
  data['navigation'] = False
  data['title'] = 'Timeout 72' 
  return render(request,'public/index.html', data)
