from __future__ import unicode_literals

from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class BookingList(models.Model):
  CHOICES = [(i,i) for i in range(0,10)]
  VOID = -1
  NONE = 0
  CONFIRMED = 1
  BLOCKED =2
  BOOKING_STATUS = (
    (VOID, 'Void'),
    (NONE, 'Not Confirmed'),
    (CONFIRMED, 'Confirm'),
    (BLOCKED, 'Blocked'),
  )
  booking_id = models.CharField(max_length = 20, null = False, blank = False)
  checkin = models.DateField(null=False)
  checkout = models.DateField(null=False)
  property = models.CharField(max_length = 300, null = True, blank = True)
  guest_name = models.CharField(max_length = 100, null = True, blank = True)
  guest_phone = PhoneNumberField(null=True, blank=True)
  booking_status = models.IntegerField(default=NONE, choices=BOOKING_STATUS, null=False)
  no_of_guest = models.IntegerField(default=1, null=False, choices=CHOICES)
  no_of_nights = models.IntegerField(default=1, null=False)
  rate = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
  total = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)