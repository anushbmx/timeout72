from .models import *
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

class BookingListAdmin(ImportExportModelAdmin):
	list_display = ['id', 'booking_id', 'checkin', 'checkout', 'booking_status', 'no_of_guest','total']

admin.site.register(BookingList,BookingListAdmin)
