from django.shortcuts import render
from .models import BookingList
# Create your views here.


@login_required
def booking_list(request):
  data = {}

  bookings = BookingList.objects.order_by('-created')

  to_show = 10
  paginator = Paginator(bookings, to_show) # Show 25 contacts per page
  count = bookings.count
  page = request.GET.get('page')
  try:
      bookings = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      bookings = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      bookings = paginator.page(paginator.num_pages)

  data['count'] = count
  data['bookings'] = bookings
  data['title'] = "Bookings"
  return render(request, 'bookinglist/bookinglist.html', data)