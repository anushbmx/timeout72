from __future__ import unicode_literals

import os
from binascii import hexlify
from django.db import models
from bookings.models import Order, Booking

def _createId():
  return hexlify(os.urandom(16))

# Create your models here.
class Payments(models.Model):
  CASH = 0
  BANK_TRANSFER = 1
  ONLINE_PAYMENT =2
  PAYMENT_METHOD = (
    (CASH, 'Cash'),
    (BANK_TRANSFER, 'Bank Transfer'),
    (ONLINE_PAYMENT, 'Online Payment'),
  )

  NOTPAID = 0
  PAID = 1
  FAILED = -1
  PAYMENT_STATUS = (
    (NOTPAID, 'Not Paid'),
    (PAID, 'Paid'),
    (FAILED, 'Failed'),
  )

  hash = models.CharField(max_length=32, default=_createId, editable=False)
  booking = models.ForeignKey(Booking, on_delete=models.PROTECT)
  order = models.ForeignKey(Order, on_delete=models.PROTECT, related_name='payments')
  total = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
  payment_method = models.IntegerField(default=2, choices=PAYMENT_METHOD, null=False)
  payment_status = models.IntegerField(default=0, choices=PAYMENT_STATUS, null=False)

  due = models.DateField(null=True)

  # Records
  created = models.DateTimeField(auto_now_add=True) 
  time = models.DateTimeField(auto_now=True)
  status = models.IntegerField(default=1, null=False)