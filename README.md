#Summary


# Requirnments
1. Python 3.4
2. Django


#Installation 
1. create a virtual environment
2. run `pip install -r requirements.txt`
3. run `./manage.py migrate`

## pre-commit

You can run `pre-commit install` to set up the pre-commit machinery which will
lint changes before you commit. Linting saves lives.


# Pillow requires the following package on Ubuntu 14.04

1. sudo apt-get install python-dev
2. sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
3. sudo apt-get install libmysqlclient-dev


# Modules used
1. Phone Number for using phone numbers
	https://github.com/stefanfoulis/django-phonenumber-field
2. Django Countries
	https://pypi.python.org/pypi/django-countries#downloads
3. Django Filter
	https://github.com/carltongibson/django-filter